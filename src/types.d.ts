export interface RectangleBounds {
    x: number,
    y: number,
    width: number,
    height: number
}

export type palletteSize = 8 | 16 | 32 | 64 | 128 | 256

export interface pxzFiles {
    name: string,
    dateLastModified: number,
    palletteSize: palletteSize,
    data: string[],
    colorPallette: string[],
    thisPath: string
}


export type transparentColor = "rgba(0, 0, 0, 0)" | "transparent";