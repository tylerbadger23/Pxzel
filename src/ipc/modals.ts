import { dialog } from "electron";
import { pxzFiles } from "../types";
import { isPxzFile } from "./pxzFiles";
export async function userSelectFolder () {

    return new Promise<Electron.OpenDialogReturnValue>( async (res, rej) => {
        const result = dialog.showOpenDialog({
            properties: ['openDirectory'],
        });

        result.catch(err => rej(err));

        result.then((results: Electron.OpenDialogReturnValue) => {
            res(results);
        });
    });
}


export async function userSelectPXZFile () {
    return new Promise<pxzFiles | null>( async (res, rej) => {
        const result = dialog.showOpenDialog({
            properties: ["openFile"],
            message: "Select the project file. MUST CONTAIN A .pxzel ending.",
            
        });

        result.catch(err => rej(err));

        result.then( async (results: Electron.OpenDialogReturnValue) => {
            if (results.canceled) res(null);

            try {
                const file = results.filePaths[0];
                const isCorrectFileType = await isPxzFile(file);
                
                if (isCorrectFileType) res(isCorrectFileType);
                else res(null);
            } catch (error) {
                res(null);
            }
        });
    });
}