import { set, get } from "electron-json-storage";
import { RectangleBounds } from "../types";
export const appSettings = {
    saveWindowBounds: saveWindowBounds,
    getWindowBounds: getWindowBounds,
}


async function saveWindowBounds (bounds: RectangleBounds) {

    set("window-location", bounds, (err) => {
        if (err) throw new Error(err);
        
        return true;
    });
}

// gets the window bounds from storage.
// returns the bounds and creates new storage opbject if bounds were nmot found.
async function getWindowBounds () {
    return new Promise<RectangleBounds>( async(res, rej) => {
        get("window-location", async (err: any, data) => {
            if (err) throw Error(err);
            
            try {
                // @ts-ignore
                if (data.x && data.y && data.height && data.width) res(data);
        
                else {
                    const newBounds: RectangleBounds = {x: 340, y: 200, width: 1000, height: 700};
                    await saveWindowBounds(newBounds);
                    res(newBounds);
                }
            } catch (error) {
                rej(error);
            }
        });
    });
    
}