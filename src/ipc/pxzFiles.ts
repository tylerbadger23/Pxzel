import { writeFile, readFile , statSync, lstat} from "fs";
import { palletteSize, pxzFiles, transparentColor } from "../types";
import randomColor from "randomcolor";

// create a new file with fs at location inside parentFolder
export async function createNewPxzFile (parentFolderPath: string, size: palletteSize) {

    const path = `${parentFolderPath}/pxzelProject${Date.now().toString()}.pxzel`;

    // generate a transparent default data layer as well.
    const defaultPxelContent: pxzFiles = {
        name: "Untitled Pxzel Project",
        data: generateDefaultPalletteValues(size),
        palletteSize: size,
        dateLastModified: Date.now(),
        thisPath: path,
        colorPallette: generatePallette() // default pallette
    }


    /// creeate  a new file at location with random name
    return new Promise<pxzFiles>( async (res, rej) => {
        writeFile(path, JSON.stringify(defaultPxelContent), function(err) {
            if(err) rej(console.log(err));

            res(defaultPxelContent);
        });  
    });
  
}

function generateDefaultPalletteValues (boardSize: number): string[] {
    const defaultBoard = new Array(boardSize * boardSize);
    let t: transparentColor = "transparent";

    for (let i = 0; i < defaultBoard.length; i ++) defaultBoard[i] = t;

    return defaultBoard;
}


export async function isPxzFile(filePath: string) {
    return new Promise <pxzFiles | null> (async (res, rej) => {
        readFile(filePath, "utf8", async (err,data) => {
            if (err) rej(null);
            
            // try to parse the file and if that works then check to make sure its corrct format
            try {
                const file: pxzFiles = JSON.parse(data);

                // check for valid properties
                if (file.name != undefined && typeof file.name == "string" && typeof file.dateLastModified == "number" && typeof file.palletteSize == "number" && typeof file.data == "object") {
                    file.thisPath = filePath;
                    res(file);
                }

                rej(null);

            } catch (error) {
                rej(null);
            }
            
          });
    });
}


// create a new file with fs at location inside parentFolder
export async function savePxzFile (file: pxzFiles) {
    const path = file.thisPath;
    
    /// creeate  a new file at location with random name
    return new Promise<pxzFiles | string>( async (res, rej) => {


        // check if path is valid and file is still in same spot.
        try {
            let exists = statSync(path);
            if (!exists) res("Path does not exist on File System: " + path);

        } catch (error) {
            rej(error);
        }
    
        try {
            // check for valid type of file. / not corrupted.
            const isValid = await isPxzFile(path);
            if (isValid == null || !isValid.data) rej("File is not of Valid Type. Did you acciden taly edit it recently?")

            // if the file is a valid type then overide it
            file.dateLastModified = Date.now();
            writeFile(path, JSON.stringify(file), function(err) {
                if(err) rej(console.log(err));
                
                res(file);
            });  
        } catch (error) {
            rej(error);
        }
    });
  
}


export function generatePallette (size?: number) {
    let s = size || Math.floor((Math.random() * 20)) + 10;

    

    const pallette = randomColor({
        luminosity: "bright",
        format: 'rgba',
        alpha: 1,
        count: s,
    });
   

    return pallette;
}