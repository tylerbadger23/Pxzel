import { ipcRenderer, contextBridge } from "electron";
import { platform, cpus } from "os";
import { pxzFiles } from "./types";
import { generatePallette } from "./ipc/pxzFiles";


const startup = {
    show: () => ipcRenderer.send("ready-to-show"),
}

const api = {
    platform: platform(),
    cpus: cpus().length,
    version: () => ipcRenderer.invoke("get-version"),
    quit: () => ipcRenderer.send("quit-app"),
    show: () => ipcRenderer.send("show-app"),
    hide: () => ipcRenderer.send("hide-app"),
    minimize: () => ipcRenderer.send("minimize-app"),
    selectFolder: () => ipcRenderer.invoke("select-folder"),
    createNewFileInFolder: (size: number) => ipcRenderer.invoke("create-new-file-in-path", size),
    selectFile: () => ipcRenderer.invoke("select-file"),
    savePxzelFile : (file: pxzFiles) => ipcRenderer.invoke("save-pxzle-file", file),
    generateColorPallette: (n?: number) => generatePallette(n),
}

contextBridge.exposeInMainWorld("startup", startup);
contextBridge.exposeInMainWorld("app", api);