import { app, BrowserWindow, ipcMain } from "electron";
import { appSettings } from "./ipc/settings";
import { userSelectFolder, userSelectPXZFile } from "./ipc/modals";
import { createNewPxzFile, savePxzFile } from "./ipc/pxzFiles";
import { palletteSize, pxzFiles } from "./types";

let editor: BrowserWindow;

app.on("ready", main);



async function main () {
    const bounds = await appSettings.getWindowBounds();

    editor = new BrowserWindow({
        width: bounds.width,
        height: bounds.height,
        x: bounds.x,
        y: bounds.y,
        show: false,
        fullscreen: false,
        maxHeight: 1074,
        maxWidth: 1920,
        minHeight: 495,
        autoHideMenuBar: true,
        webPreferences: {
            preload: __dirname + "/preload.js",
            devTools: true,
        },
        roundedCorners: true,
        
    });

    editor.setAspectRatio(1000/ 700);
    editor.loadFile("./editor.html");
    editor.webContents.openDevTools();

    // Handle moving the window and the size
    editor.on("resized", async () => { appSettings.saveWindowBounds(editor.getBounds())});
    editor.on("moved", async () => { appSettings.saveWindowBounds(editor.getBounds())});
    
    // Handlew IPC Calls
    ipcMain.on('ready-to-show', () => editor.show());
    ipcMain.on("show-app", () => editor.show());
    ipcMain.on("hide-app", () => editor.hide());    

}


ipcMain.handle("create-new-file-in-path", async (h, a) => {
    let size:palletteSize = a;

    if (!a) size = 8;
    // have user select folder for new file
    const folderLocation = await userSelectFolder();

    // check for user canceled
    if (folderLocation.canceled) {
        return null;

    } else {
        const folderPath = folderLocation.filePaths[0];
        
        // create a default file at this location to be used as a Project.pxzel file.
        try {
            const fileOBJ = await createNewPxzFile(folderPath, size);
            return fileOBJ;
            // if an error then just return null and handle it in the renderer
        } catch (error) {
            console.log(error);
            return null;
        }
    }
    
}); 


// open a file from the system
// Must make sure its a pxzel file xD
ipcMain.handle("select-file", async () => {
    const file = await userSelectPXZFile();

    if (file) return file;
    
    else return null;
});

// File needs to be saved. Take args and save it to its proper destination.
// Return null if errors occur or return the error text.
// Return true if success.
ipcMain.handle("save-pxzle-file", async (handler, args) => {
    const file: pxzFiles = args;

    const saveResult = savePxzFile(file);

    return saveResult;

});