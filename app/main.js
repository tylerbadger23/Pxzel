"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var settings_1 = require("./ipc/settings");
var modals_1 = require("./ipc/modals");
var pxzFiles_1 = require("./ipc/pxzFiles");
var editor;
electron_1.app.on("ready", main);
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var bounds;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, settings_1.appSettings.getWindowBounds()];
                case 1:
                    bounds = _a.sent();
                    editor = new electron_1.BrowserWindow({
                        width: bounds.width,
                        height: bounds.height,
                        x: bounds.x,
                        y: bounds.y,
                        show: false,
                        fullscreen: false,
                        maxHeight: 1074,
                        maxWidth: 1920,
                        minHeight: 495,
                        autoHideMenuBar: true,
                        webPreferences: {
                            preload: __dirname + "/preload.js",
                            devTools: true,
                        },
                        roundedCorners: true,
                    });
                    editor.setAspectRatio(1000 / 700);
                    editor.loadFile("./editor.html");
                    editor.webContents.openDevTools();
                    // Handle moving the window and the size
                    editor.on("resized", function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                        settings_1.appSettings.saveWindowBounds(editor.getBounds());
                        return [2 /*return*/];
                    }); }); });
                    editor.on("moved", function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                        settings_1.appSettings.saveWindowBounds(editor.getBounds());
                        return [2 /*return*/];
                    }); }); });
                    // Handlew IPC Calls
                    electron_1.ipcMain.on('ready-to-show', function () { return editor.show(); });
                    electron_1.ipcMain.on("show-app", function () { return editor.show(); });
                    electron_1.ipcMain.on("hide-app", function () { return editor.hide(); });
                    return [2 /*return*/];
            }
        });
    });
}
electron_1.ipcMain.handle("create-new-file-in-path", function (h, a) { return __awaiter(void 0, void 0, void 0, function () {
    var size, folderLocation, folderPath, fileOBJ, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                size = a;
                if (!a)
                    size = 8;
                return [4 /*yield*/, modals_1.userSelectFolder()];
            case 1:
                folderLocation = _a.sent();
                if (!folderLocation.canceled) return [3 /*break*/, 2];
                return [2 /*return*/, null];
            case 2:
                folderPath = folderLocation.filePaths[0];
                _a.label = 3;
            case 3:
                _a.trys.push([3, 5, , 6]);
                return [4 /*yield*/, pxzFiles_1.createNewPxzFile(folderPath, size)];
            case 4:
                fileOBJ = _a.sent();
                return [2 /*return*/, fileOBJ];
            case 5:
                error_1 = _a.sent();
                console.log(error_1);
                return [2 /*return*/, null];
            case 6: return [2 /*return*/];
        }
    });
}); });
// open a file from the system
// Must make sure its a pxzel file xD
electron_1.ipcMain.handle("select-file", function () { return __awaiter(void 0, void 0, void 0, function () {
    var file;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, modals_1.userSelectPXZFile()];
            case 1:
                file = _a.sent();
                if (file)
                    return [2 /*return*/, file];
                else
                    return [2 /*return*/, null];
                return [2 /*return*/];
        }
    });
}); });
// File needs to be saved. Take args and save it to its proper destination.
// Return null if errors occur or return the error text.
// Return true if success.
electron_1.ipcMain.handle("save-pxzle-file", function (handler, args) { return __awaiter(void 0, void 0, void 0, function () {
    var file, saveResult;
    return __generator(this, function (_a) {
        file = args;
        saveResult = pxzFiles_1.savePxzFile(file);
        return [2 /*return*/, saveResult];
    });
}); });
