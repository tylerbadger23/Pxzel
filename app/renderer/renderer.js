import { api } from "./api.js";
import { Editor } from "./classes/editor.js";
import { Project } from "./classes/projectStorage.js";
import { selectProjectFileToOpen, selectFolderLocationAndCreateFile } from "./classes/files.js";
import { State, openBTN_DOM, newBTN_DOM } from './classes/state.js';

const titleText = document.getElementById("proj-name-input");
const saveBtn = document.getElementById("save-btn-editor");

const project = new Project();
const editor = new Editor();

Startup();

// shows the welcome screen where the user can create a new project or open a existing one.
async function Startup () {
    api.show();
    State.loadWelcomeScreen();

    // handle click events for each btn
    openBTN_DOM.addEventListener("click", async () => {
    
        selectProjectFileToOpen().then(fileDataObj => {
            console.log(fileDataObj)
            
            if (fileDataObj) {
                project.loadProjectFile(fileDataObj);
                State.hideWelcome();
                State.loadMain();
                loadEditor();
            } else console.log(fileDataObj + " -- aka user canceled or file was not correct type");
        }).catch((err) => {
            console.error(err);
        });
    });


    /// creayena new Project
    newBTN_DOM.addEventListener("click", async () => {
        selectFolderLocationAndCreateFile().then(fileDataObj => {

            if (fileDataObj) {
                project.loadProjectFile(fileDataObj);
                State.hideWelcome();
                State.loadMain();
                loadEditor();
                
            } else console.log(fileDataObj + " -- aka user canceled or file was not correct type");
        }).catch((err) => {
            console.error(err);
        });

    });
}


async function loadEditor () {
    const projData = project.data();

    // change the page titlebar
    State.updatePageTitle(projData.name);

    // load the project to the Editor class
    // and render the tiles 
    editor.loadProject(projData);
    await editor.render(projData);
    let name = project.getName();
    titleText.value = name;
    window.addEventListener("resize", async () => {
        // on each resize we need to redraw the dom. So we dont loose the pixel information
        // we will save its state and then pass it back into the function
        // as new data.

        // Finally we can call render again.
        const data = editor.getStateOfData();
        project.updateProjectData(data);
        console.log(data);
        editor.loadData(data);
        editor.render()
    });
}



titleText.addEventListener("change", () => {
    project.setName(titleText.value);
});

saveBtn.addEventListener("click", (e) => {
    saveEverything();
});


export function saveEverything () {
    const datas = editor.getStateOfData();
    const pallettedata = editor.getPalletteData();

    project.updateProjectData(datas, pallettedata);
    project.save();
}