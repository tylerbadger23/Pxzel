export class PalletteManager {
    #addPalletteButton = document.getElementById("add-pallette-btn");
    #palleteDOM = document.getElementById("pallette");
    #activeColor = "rgba(0,250,154, 1)"; // default to summergreen
    #activeTile = null;
    
    #pallette = ["rgba(0,250,154, 1)"];

    // collection of dom andf color elements
    #colorsObjectCollection = [];
    // tiles DOM elemnents go here

    constructor () {
        this.#addPalletteButton.addEventListener("click", () => {
            console.log("add pallete button clicked");
        });
    }

    getActive () {
        return this.#activeColor;
    }

    getPallette () {
        return this.#pallette;
    }

    setPallette (pallette = ["rgba(0,250,154, 1)"]) {
        if (pallette.length == 0) pallette = ["rgba(0,250,154, 1)"];
        this.#pallette = pallette;
        this.#activeColor = pallette[0];
    }


    renderPallette () {
        // delete each obnject to prevenet memory leak from listeners growing
        this.#colorsObjectCollection.forEach((elem) => {
            elem.obj.remove()
        });


        this.#colorsObjectCollection = [];
        this.#palleteDOM.innerHTML = '';

        for (let color of this.#pallette) {
            const tile = createTile(color);
            
            tile.addEventListener("mousedown", (e) => {
                this.#handleTileClick(tile, color, e);
            });

            this.#colorsObjectCollection.push({color: color, obj: tile})
            this.#palleteDOM.appendChild(tile);
        }
    }

    // Search Array and remove the element from, the dom and from both global arrays.
    #deleteTile (tileOBJ) {
        const newTileArray = [];
        const newPalletteArray = [];
        let count = 0;

        // find the corresponding tileObject and check if it matches each iteration
        for (let item of this.#colorsObjectCollection) {

            // if they DONT match then set the new active color.
            // add the color to a new mutates pallette
            if (!tileOBJ.isSameNode(item.obj)) {
                newTileArray.push(item);
                this.#activeColor = 'rgba(0,0,0,1)';
                this.#activeTile = null;
                newPalletteArray.push(item.color);
            }
            
            // they are the same so delete it.
            else {
                item.obj.remove();

                // if its one then set black as default
            if (this.#colorsObjectCollection.length == 1) 
                this.#activeColor = 'rgba(0,0,0,1)';
                this.#activeTile = null;
            }

            count += 1;
        }

        this.#colorsObjectCollection = newTileArray;
        this.#pallette = newPalletteArray;

        this.renderPallette();
    }

    #selectTileAsActive (tileOBJ, color) {

        if (this.#activeTile || this.#activeTile !== null) {
            this.#activeTile.style.borderColor = "rgba(255, 215, 0, 0)"; // remove border
        }

        this.#activeColor = color;
        tileOBJ.style.borderColor = "rgba(255, 215, 0, 1)";
        this.#activeTile = tileOBJ;
        
    }

    /**
     * Right click will delete tile from class as well as settings. Left Click selects color
     */
    #handleTileClick (tile, color, eventVal) {
        
        const btnClicked = eventVal.button;

        if (btnClicked == 0) { // left click
            this.#selectTileAsActive(tile, color);
        } else {
            this.#deleteTile(tile);
        }
    }
    
}


function createTile (color) {
    const tile = document.createElement("div");

    tile.classList.add("color-tile")
    tile.style.backgroundColor = color;
    tile.style.borderStyle = "solid";
    tile.style.borderWidth = "1px";
    tile.style.cursor = "pointer";
    tile.style.borderColor = "rgba(255, 215, 0, 0)"; // transparent at first


    return tile;
}
