const welcomeDOM = document.getElementById("welcome");
const appDOM = document.getElementById("app");
export const openBTN_DOM = document.getElementById("open-project-btn");
export const newBTN_DOM = document.getElementById("new-project-btn");


export const State = {
    loadWelcomeScreen: () => loadWelcomeScreen(),
    loadMain: () => loadMain(),
    hideWelcome:() => hideWelcome(),
    updatePageTitle: (title) => updatePageTitle(title),
}

function loadWelcomeScreen () {
    welcomeDOM.style.display = "block";
}

function loadMain () {
    appDOM.style.display = "grid";
}

function hideWelcome () {
    welcomeDOM.style.display = "none";
}

function updatePageTitle (projectName = "Untitled project") {
    document.head.title = `PXzel -  ${projectName}`;
}
