export class ToolManager {
    #erasing = false;
    #erasingDOM = document.getElementById("select-erase");
    #brushDOM = document.getElementById("select-brush");

    constructor () {
        this.#erasingDOM.addEventListener("click", (e) => {
            this.#erasing = true;
            removeSelectedStateFromTools(this.#erasingDOM, this.#brushDOM);
            addSelectedState(this.#erasingDOM);
        });

        this.#brushDOM.addEventListener("click", (e) => {
            this.#erasing = false;
            removeSelectedStateFromTools(this.#erasingDOM, this.#brushDOM);
            addSelectedState(this.#brushDOM);
        });
    }

    erase (v) {
        if (typeof v != "boolean") v = true;
        this.#erasing = v;
    }

    paint (e) {
        if (typeof e != "boolean") e = false;
        this.#erasing = false
    }

    isErasing () {
        return this.#erasing;
    }

    isPainting () {
        return !this.#erasing;
    }


}

function removeSelectedStateFromTools(erase, brush) {
    erase.classList.remove("tool-selected");
    brush.classList.remove("tool-selected");
}

function addSelectedState(domelem) {
    domelem.classList.add("tool-selected");
}