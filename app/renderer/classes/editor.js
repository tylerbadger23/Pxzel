import { ToolManager } from "./tooling.js";
import { PalletteManager } from "./pallette.js";
import { saveEverything } from "../renderer.js";

export class Editor {
    #editorLines = true;
    #tiles = [];
    #editor = document.getElementById("editor");
    #projectFile = {name: "e", data: ['transparent'], dateLastModified: 1000, palletteSize: 8, thisPath: "c", colorPallette: ['rgba(0,250,154, 1)', 'rgba(250,0,154, 1)']}; // default pallette

    #dissableGridButton = document.getElementById("dissable-grid");
    #enableGridButton = document.getElementById("enable-grid");

    constructor () {

        // handle editor related UI buttons
        this.#dissableGridButton.addEventListener("click", (e) => {
            this.#editorLines = false;
            this.#dissableGridButton.classList.add("tool-selected");
            this.#enableGridButton.classList.remove("tool-selected");
            setLineStyle(this.#tiles, this.#editorLines);
        });

        this.#enableGridButton .addEventListener("click", (e) => {
            this.#editorLines = true;
            this.#enableGridButton.classList.add("tool-selected");
            this.#dissableGridButton.classList.remove("tool-selected");
            setLineStyle(this.#tiles, this.#editorLines);
        });
    }

    // Manage Tool state eg: brush size, erase, etc
    #tool = new ToolManager();

    // Manage Pallette State: eg colors active, etc
    #pallette = new PalletteManager();

    /**
     * Given a project file data mutate the origional data and overide its state. Usually you wanna
     * cvall this.render() after this function finished.
     */
    loadProject (projectFile) {
        this.#projectFile = projectFile;
        this.#pallette.setPallette (this.#projectFile.colorPallette);
        this.#pallette.renderPallette ();
    }

    loadData(data = ['']) {
        this.#projectFile.data = data;
    }
    /**
     * Will Re-Generate Tiles and Clear DOM. Generates the entire tileset and force-updates DOM.
     * This is Rendering intensive for large tilesets. 256x256 plus could 
     * take a few hundren miliseconds
     * Also has to delete all event listeners and create new ones.
     */
    async render() {
        console.time("render");
        // clear event listeners from all children and stuffz.
        // Without removing tiles event listeners or deleting them it will/ could cause memory
        // Isue
        if (this.#tiles.length > 0) {
            this.#tiles.forEach((tile) => {
                tile.remove();
            });
        }
        // clear DOM
        this.#editor.innerHTML = "";
        
        // create new tiles now
        let palSize = this.#projectFile.palletteSize;
        const tiles = [];
        const tileSize = determineTileSize(this.#editor.getBoundingClientRect(), palSize);
        const rows = [];
        let count = 0;
        // linearly create each tile and its dom elemtn
        for (let i = 0; i < palSize; i++) {
            const row = document.createElement("div");
            row.classList.add("tile-row");

            for (let z = 0; z < palSize; z++) {
                const tileColor = this.#projectFile.data[count];
                const tile = document.createElement("div");
                tile.style.backgroundColor = tileColor;
                tile.classList.add("tile");
                tile.style.width = `${tileSize}px`;
                tile.style.height = `${tileSize}px`;
                

                let alpha = (this.#editorLines)? 1 : 0;
                tile.style.borderColor = `rgba(0, 0, 0, ${alpha})`;
                tile.style.cursor = "pointer";
                tile.style.borderStyle = `solid`;
                tile.style.borderWidth = `1px`;
                // handle clicks
                tile.addEventListener('click', () => this.#onTileClicked(tile));
                tile.addEventListener("dragover", (ev) => this.#onTileDraggedOver(ev, tile));

                tiles[count] = tile;
                row.appendChild(tile);
                count += 1;
            }

            rows.push(row);
            this.#editor.appendChild(row);
        }

        // Overide the new tiles data.
        this.#tiles = tiles;

        console.timeEnd("render");
    }

    /**
     * Returns a string[] with the exact color data for each tile in the scene. This is neeeded to save the
     * Project through the Project.save()
     */
    getStateOfData () {
        // create a new array of size tiles.
        let linearData = [''];

        // loop through list of tile elements
        for (let v = 0;v < this.#tiles.length; v++) {
            const pixelColor = this.#tiles[v].style.backgroundColor.toString();
            // push color to a new aray and return in to be seved.
            linearData[v] = pixelColor;
        }

        return linearData;
    }

    /**
     * Set the state for the pallette from the Pallette class. Used before saving.
     */
    getPalletteData () {

        let newPallette = this.#pallette.getPallette();
        this.#projectFile.colorPallette = newPallette

        return newPallette;
    }

    /**
     * Determine What to do if a tile is clicked on. Check if were erasing or painting as 
     * well as color.
    */
    #onTileClicked (objClicked) {
        // if painting then color in the objectm with the appropriet color.
        if (this.#tool.isPainting()) {
            const activeColor = this.#pallette.getActive();
            colorTile(objClicked, activeColor);
        }
        
        // erase the tile if the tool is on erase mode.
        else eraseTile(objClicked);
        
    }

    /**
     * Determine if while dragging you need to erase or color tile.
    */
     #onTileDraggedOver (ev, objClicked) {
         ev.preventDefault();
        // if painting then color in the objectm with the appropriet color.
        if (this.#tool.isPainting()) {
            const activeColor = this.#pallette.getActive();
            colorTile(objClicked, activeColor);
        }
        
        // erase the tile if the tool is on erase mode.
        else eraseTile(objClicked);
        
    }
}

/**
 * Erase the color inside a specific tile. Simply erases the bg color
 * force-redraws the DOM.
 */
function eraseTile (tileObj) {
    tileObj.style.backgroundColor = "transparent";
}

/**
 * Change the background color of the tile object passed in. force-redraws the DOM.
 */
function colorTile (tileObj, color) {
    tileObj.style.backgroundColor = color;
}

// Determine Cell Size for a perfect square layout.
function determineTileSize (space, tileCount) {
    // find which is smallest and flor it.
    // This will the be used as the width and height of each tile in out grid

    // this will find the largest tile that can be used with any space as
    // its gap approaches -> 0
    const tileSizeW = ((space.width - 2) / tileCount);
    const tileSizeH = ((space.height - 10) / tileCount);
    let size = 0;

    if (tileSizeW < tileSizeH) {
        size =  Math.floor(tileSizeW);
    } else {
        size =  Math.floor(tileSizeH);
    }
    return size;
    
}


function setLineStyle (linesArr, transparent, color = [0, 0, 0], style = 'solid', pxWidth = 1) {
    linesArr.forEach(tile => {
        let alpha = (transparent)? 1 : 0;
        tile.style.borderColor = `rgba(${color[0]},${color[1]}, ${color[2]}, ${alpha})`;
        tile.style.borderStyle = `${style}`;
        tile.style.borderWidth = `${pxWidth}px`;
    });
}