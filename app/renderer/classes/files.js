import { api } from "../api.js";

/**
 * Creates a popup window and then returns the proper file if it exists and its Project Class.
 * if It fails it will return null.
 */
export async function selectProjectFileToOpen () {
    const fileDatas = await api.files.selectFile();

    return fileDatas;
}

export async function selectFolderLocationAndCreateFile () {
    const folderInfos = await api.files.createNewFileInFolder();
    return folderInfos;
}

export class FileManager {

}