import { api } from "../api.js";

export class Project {
    #needsUpdate = false;
    #projectFile = null;

    constructor (projectSettings) {
        this.#projectFile = projectSettings || null;
    }

    /**
     * Load project file into state.
     * @param {*} project the .prxzel file state 
     */
    loadProjectFile (project) {
        this.#projectFile = project;
    }

    /**
     * Load project file into state.
     * @param {*} project the .prxzel file state 
     */
     updateProjectData (datas = [''], pallette = ['']) {
        this.#projectFile.colorPallette = pallette;
        this.#projectFile.data = datas;
        this.#needsUpdate = true;
    }

    /**
     * Tries to save the project file using ipc. If success and saved to the 
     * users machine will remove the need to update to false
     * will Alert user if file was not saved  for now.
     */
    async save () {
        const success = await api.project.savePxzelFile(this.#projectFile);
        console.log(success)
        if (success == undefined || typeof success == "string" || typeof success != "object") {
            
            console.error(success);
            alert("saving file was not successfull");
        } else {
            this.#needsUpdate = false;
        }
    }


    /// build the project basically to a image png etc
    async export () {
        
    }


    // return the data
    data () {
        return this.#projectFile;
    }

    getName () {
        if (this.#projectFile.name) return this.#projectFile.name;
        else return null;
    }

    setName (newName = false) {
        if(!newName) return this.getName();

        this.#projectFile.name = newName;
        this.#needsUpdate = true;
    }
}