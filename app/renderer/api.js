
// The is the API used by the contextBridge
export const api = {
   show: () => window.startup.show(),
   appVersion: window.app.version,
   platform: window.app.platform,
   state: {
      exitApp: () => window.app.quit(),
      hideApp: () => window.app.hide(),
      showApp: () => window.app.show(),
      minimzeApp: () => window.app.minimize(),
   },
   files: {
      selectFolder: () => window.app.selectFolder(),
      selectFile: () => window.app.selectFile(),
      createNewFileInFolder: (size) => window.app.createNewFileInFolder(size),
   },
   project: {
      savePxzelFile: (file) => window.app.savePxzelFile(file),
   },
   generateColorPallette: (n) => window.app.generateColorPallette(n),

}