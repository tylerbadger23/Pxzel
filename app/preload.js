"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var os_1 = require("os");
var pxzFiles_1 = require("./ipc/pxzFiles");
var startup = {
    show: function () { return electron_1.ipcRenderer.send("ready-to-show"); },
};
var api = {
    platform: os_1.platform(),
    cpus: os_1.cpus().length,
    version: function () { return electron_1.ipcRenderer.invoke("get-version"); },
    quit: function () { return electron_1.ipcRenderer.send("quit-app"); },
    show: function () { return electron_1.ipcRenderer.send("show-app"); },
    hide: function () { return electron_1.ipcRenderer.send("hide-app"); },
    minimize: function () { return electron_1.ipcRenderer.send("minimize-app"); },
    selectFolder: function () { return electron_1.ipcRenderer.invoke("select-folder"); },
    createNewFileInFolder: function (size) { return electron_1.ipcRenderer.invoke("create-new-file-in-path", size); },
    selectFile: function () { return electron_1.ipcRenderer.invoke("select-file"); },
    savePxzelFile: function (file) { return electron_1.ipcRenderer.invoke("save-pxzle-file", file); },
    generateColorPallette: function (n) { return pxzFiles_1.generatePallette(n); },
};
electron_1.contextBridge.exposeInMainWorld("startup", startup);
electron_1.contextBridge.exposeInMainWorld("app", api);
