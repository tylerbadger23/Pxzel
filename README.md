# Pxzel 
### Completely offline, free and easy to use pixel art tool

Create pixel art fast with intuitive controls and fast good performance. Then export your work as a png and repeat. To save or edit the file on another device, it's as simple as making a copy of the .pxzel file and opening it up.

## Features
* Theme Support and Customizable Pannels
* Export to png.
* Intuitive clutter free UI.
* Easy to use file-type.
* Free with Updates and Additonal Support and features being added all the time.


**************** Work in progress and is subject to minor bugs and headaches. ************* 
